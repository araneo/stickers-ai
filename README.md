# stickers-ai

Com a popularização do uso de inteligência artificial para geração de, entre outras coisas, textos, se torna importante que os autores identifiquem se houve ou não a utilização desta tecnologia em sua obra. Por uma questão estética, elaborei stickers para utilizar em meus sites para essa identificação e disponibilizo aqui a quem possa interessar.

Deixo dois modelos diferentes para que poss esolher o que mais lhe agrada. Cada opção com três opções de cor. Você poderá também (caso nenhuma opção de cor lhe agrade) baixar a versão colorida e editá-la em um editor de texto substituindo as ocorrências de #3838c7 pela cor que deseja no fundo e #ffb788 pela cor que deseja no texto (as cores devem estar em valores hexadecimais... caso não saiba como encontar esse valores é só consultar o [este site](https://www.w3schools.com/colors/colors_picker.asp).).

## Escrito sem ajuda de inteligência artificial

### estilo selo

![feito por um ser humano sem ajuda de inteligência artificial - quadrado fundo preto](img/q-humano-p.svg "feito por um ser humano sem ajuda de inteligência artificial - quadrado fundo preto") ![feito por um ser humano sem ajuda de inteligência artificial - quadrado fundo branco](img/q-humano-b.svg "feito por um ser humano sem ajuda de inteligência artificial - quadrado fundo colorido") ![feito por um ser humano sem ajuda de inteligência artificial - quadrado fundo preto](img/q-humano-c.svg "feito por um ser humano sem ajuda de inteligência artificial - quadrado fundo colorido")

### estilo sticker

![feito por um ser humano sem ajuda de inteligência artificial - sticker fundo preto](img/s-humano-p.svg "feito por um ser humano sem ajuda de inteligência artificial - sticker fundo preto") ![feito por um ser humano sem ajuda de inteligência artificial - sticker fundo branco](img/s-humano-b.svg "feito por um ser humano sem ajuda de inteligência artificial - sticker fundo colorido") ![feito por um ser humano sem ajuda de inteligência artificial - sticker fundo preto](img/s-humano-c.svg "feito por um ser humano sem ajuda de inteligência artificial - sticker fundo colorido")

## Escrito com ajuda de inteligência artificial

### estilo selo

![feito por um ser humano com ajuda de inteligência artificial - quadrado fundo preto](img/q-humanoeia-p.svg "feito por um ser humano com ajuda de inteligência artificial - quadrado fundo preto") ![feito por um ser humano com ajuda de inteligência artificial - quadrado fundo branco](img/q-humanoeia-b.svg "feito por um ser humano com ajuda de inteligência artificial - quadrado fundo colorido") ![feito por um ser humano com ajuda de inteligência artificial - quadrado fundo preto](img/q-humanoeia-c.svg "feito por um ser humano secom ajuda de inteligência artificial - quadrado fundo colorido")

### estilo sticker

![feito por um ser humano com ajuda de inteligência artificial - sticker fundo preto](img/s-humanoeia-p.svg "feito por com ser humano com ajuda de inteligência artificial - sticker fundo preto") ![feito por um ser humano com ajuda de inteligência artificial - sticker fundo branco](img/s-humanoeia-b.svg "feito por um ser humano com ajuda de inteligência artificial - sticker fundo colorido") ![feito por um ser humano com ajuda de inteligência artificial - sticker fundo preto](img/s-humanoeia-c.svg "feito por um ser humano com ajuda de inteligência artificial - sticker fundo colorido")

## Escrito por uma inteligência artificial

### estilo selo

![feito para um ser humano com através de uma inteligência artificial - quadrado fundo preto](img/q-ia-p.svg "feito para um ser humano com através de uma inteligência artificial - quadrado fundo preto") ![feito para um ser humano com através de uma inteligência artificial - quadrado fundo branco](img/q-ia-b.svg "feito para um ser humano com através de uma inteligência artificial - quadrado fundo colorido") ![feito para um ser humano com através de uma inteligência artificial - quadrado fundo preto](img/q-ia-c.svg "feito para um ser humano com através de uma inteligência artificial - quadrado fundo colorido")

### estilo sticker

![feito para um ser humano com através de uma inteligência artificial - sticker fundo preto](img/s-ia-p.svg "feito para um ser humano com através de uma inteligência artificial - sticker fundo preto") ![feito para um ser humano com através de uma inteligência artificial - sticker fundo branco](img/s-ia-b.svg "feito para um ser humano com através de uma inteligência artificial - sticker fundo colorido") ![feito para um ser humano com através de uma inteligência artificial - sticker fundo preto](img/s-ia-c.svg "feito para um ser humano com através de uma inteligência artificial - sticker fundo colorido")

## Outras opções

Este projeto foi parcialmente inspirado pelo [Not By AI](https://notbyai.fyi/). Eles disponibilizam um badge para marcar obras feitas por humanos.

## Considerações

Devo deixar aqui um agradecimento à Rosana Pimenta pelos conselhos no design.

Caso tenha alguma sugestão, crítica ou deseja divulgar sua própria versão de badge entre em contato comigo!

## Licença
Os stickers são livres para uso com ou sem modificações da forma que quiser. Estão sob uma licença [CC0 1.0](LICENSE).